USE aptunes;
DROP PROCEDURE IF EXISTS MockAlbumRelease;

delimiter $$
CREATE PROCEDURE MockAlbumRelease(OUT succes BOOL)
BEGIN
DECLARE numberOfAlbums INT DEFAULT 0;
DECLARE numberOfBands INT DEFAULT 0;
DECLARE randomAlbumId INT DEFAULT 0;
DECLARE randomBandId INT DEFAULT 0;
SELECT count(*) INTO numberOfAlbums FROM Albums;
SELECT count(*) INTO numberOfBands FROM Bands;
SET randomAlbumId = FLOOR(RAND() * numberOfAlbums) + 1;
SET randomBandId = FLOOR(RAND() * numberOfBands) + 1;
IF (randomBandId, randomAlbumId) NOT IN (SELECT * FROM albumreleases) THEN
	INSERT INTO albumreleases(Bands_Id, Albums_Id)
    VALUES(randomBandId, randomAlbumId);
    SET succes = 1;
ELSE
	SET succes = 0;
END IF;
END $$
delimiter ;