USE ModernWays;
CREATE TABLE IF NOT EXISTS Uitleningen (
Leden_Id INT NOT NULL,
Boeken_Id INT,
Startdatum DATE NOT NULL,
Einddatum DATE,
CONSTRAINT fk_Uitleningen_Leden FOREIGN KEY (Leden_Id) 
REFERENCES Leden(Id),
CONSTRAINT fk_Uitleningen_Boeken FOREIGN KEY (Boeken_Id)
REFERENCES Boeken(Id)
);
