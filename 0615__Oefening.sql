USE ModernWays;
ALTER VIEW auteursboeken
AS
SELECT concat(personen.Voornaam, " ", personen.Familienaam) AS "Auteurs", boeken.Titel, boeken.Id AS "Boeken_Id"
FROM boeken 
INNER JOIN publicaties ON Boeken_Id = Boeken.Id
INNER JOIN personen ON Personen_Id = Personen.Id;

