USE aptunes;
DELIMITER $$
CREATE PROCEDURE GetLiedjes (IN zoekvar VARCHAR(50))
BEGIN
	SELECT Titel
    FROM Liedjes
    WHERE Titel LIKE concat('%',zoekvar,'%');
END$$
DELIMITER ;