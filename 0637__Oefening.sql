USE ModernWays;
-- Tabel personen verwijderen indien deze bestaat en het aanmaken ervan
DROP TABLE IF EXISTS Personen;
CREATE TABLE Personen (
	Voornaam VARCHAR(255) NOT NULL,
    Familienaam VARCHAR(255) NOT NULL
);
-- De kolom Voornaam en Familienaam kopieren van tabel Boeken in de tabel Personen
INSERT INTO Personen (Voornaam, Familienaam)
	SELECT DISTINCT Voornaam, Familienaam FROM Boeken;
-- De resterende andere kolommen aan tabel personen toevoegen
ALTER TABLE Personen ADD (
	Id INT AUTO_INCREMENT PRIMARY KEY,
    AanspreekTitel VARCHAR(30) NULL,
    Straat VARCHAR(80) NULL,
    Huisnummer VARCHAR(5) NULL,
    Stad VARCHAR(50) NULL,
    Commentaar VARCHAR(100) NULL,
    Biografie VARCHAR(400) NULL
);
-- Een foreign key kolom aanmaken in de tabel boeken
ALTER TABLE Boeken ADD Personen_Id INT NULL;
-- Linken van personen en boeken op basis van kolommen Voornaam en Familienaam die we zojuist gekopieerd hebben.
/* SELECT Boeken.Voornaam,
   Boeken.Familienaam,
   Boeken.Personen_Id,
   Personen.Voornaam,
   Personen.Familienaam,
   Personen.Id
FROM Boeken CROSS JOIN Personen
WHERE Boeken.Voornaam = Personen.Voornaam AND
    Boeken.Familienaam = Personen.Familienaam; */
SET SQL_SAFE_UPDATES = 0;
UPDATE Boeken CROSS JOIN Personen
    SET Boeken.Personen_Id = Personen.Id
WHERE Boeken.Voornaam = Personen.Voornaam AND
    Boeken.Familienaam = Personen.Familienaam;
SET SQL_SAFE_UPDATES = 1;
-- Not null constraint toevoegen aan de kolom Personen_Id in de tabel Boeken
ALTER TABLE Boeken CHANGE Personen_Id Personen_Id INT NOT NULL;
-- Dubbele kolommen verwijderen uit de tabel Boeken
ALTER TABLE Boeken DROP COLUMN Voornaam,
	DROP COLUMN Familienaam;
-- De foreign key constraint op de kolom Personen_Id toevoegen
ALTER TABLE Boeken ADD CONSTRAINT fk_Boeken_Personen
	FOREIGN KEY(Personen_Id) REFERENCES Personen(Id);
    
select * from Boeken;