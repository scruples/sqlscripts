USE aptunes;
DELIMITER $$
CREATE PROCEDURE NumberOfGenres (OUT aantal TINYINT)
BEGIN
	SELECT count(*)
    INTO aantal
    FROM Genres;
END $$
DELIMITER ;