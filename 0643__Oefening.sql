USE aptunes;
DELIMITER $$
CREATE PROCEDURE CreateAndReleaseAlbum (IN titel VARCHAR(100), IN bands_Id INT)
BEGIN
START TRANSACTION;
INSERT INTO Albums (Titel)
VALUES(titel);
INSERT INTO albumreleases (Bands_Id, Albums_Id)
VALUES(bands_Id, LAST_INSERT_ID());
COMMIT;
END$$
DELIMITER ;