USE ModernWays;
CREATE TABLE IF NOT EXISTS Metingen(
Tijdstip DATETIME NOT NULL,
Grootte SMALLINT UNSIGNED NOT NULL,
Marge DECIMAL(5,2)
);