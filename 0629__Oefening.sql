SELECT Studenten_Id
FROM Evaluaties
INNER JOIN Studenten ON Studenten_Id = Studenten.Id
GROUP BY Studenten.Id
HAVING AVG(Cijfer) > (SELECT AVG(cijfer) FROM Evaluaties);
-- Waarom moeten we hier een join gebruiken? Had het eerst zonder gedaan en kom op zelfde uitkomst? 