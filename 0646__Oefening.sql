USE aptunes;
DROP PROCEDURE IF EXISTS MockAlbumReleases
delimiter $$

CREATE PROCEDURE MockAlbumReleases(IN extraReleases INT)
BEGIN
DECLARE counter INT DEFAULT 0;
DECLARE succes BOOL;
REPEAT
	CALL MockAlbumReleaseWithSucces(succes);
    IF succes = 1 THEN
    SET counter = counter + 1;
    END IF;
UNTIL counter = extraReleases
END repeat;
END$$
delimiter ;