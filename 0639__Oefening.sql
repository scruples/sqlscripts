USE ModernWays;
INSERT INTO Personen (Voornaam, Familienaam, AanspreekTitel)
VALUES ('Jean-Paul', 'Satre', 'Meneer');
INSERT INTO Boeken (Titel, Verschijningsdatum, Uitgeverij, Personen_Id)
VALUES ('De Woorden', 1961, 'De Bezige Bij', 
(SELECT Id FROM Personen WHERE Familienaam = 'Satre' AND Voornaam = 'Jean-Paul')
);