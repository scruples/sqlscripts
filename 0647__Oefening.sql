USE aptunes;
DROP PROCEDURE IF EXISTS MockAlbumReleasesLoop
delimiter $$

CREATE PROCEDURE MockAlbumReleasesLoop(IN extraReleases INT)
BEGIN
DECLARE counter INT DEFAULT 0;
DECLARE succes BOOL;
callLoop : loop
	CALL MockAlbumReleaseWithSucces(succes);
    IF succes = 1 THEN
    SET counter = counter + 1;
    END IF;
IF counter = extraReleases THEN
	LEAVE callLoop;
END IF;
END LOOP;
END$$
delimiter ;