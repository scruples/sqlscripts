USE ModernWays;
CREATE VIEW AuteursBoeken AS
SELECT concat(personen.Voornaam, " ", personen.Familienaam) AS "Auteurs", boeken.Titel
FROM boeken 
INNER JOIN publicaties ON Boeken_Id = Boeken.Id
INNER JOIN personen ON Personen_Id = Personen.Id;