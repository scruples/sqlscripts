SELECT Leden.Voornaam, Boeken.Titel
FROM uitleningen
INNER JOIN Boeken ON Boeken.Id = uitleningen.Boeken_Id
INNER JOIN Leden On Leden.Id = uitleningen.Leden_Id