USE aptunes;
DELIMITER $$
CREATE PROCEDURE CleanupOldMemberships(IN enddate DATE, OUT num INT)
BEGIN
START TRANSACTION;
SELECT count(*) INTO num
FROM lidmaatschappen
WHERE Lidmaatschappen.Einddatum IS NOT NULL AND lidmaatschappen.Einddatum < enddate;
SET Sql_safe_updates = 0;
DELETE
FROM Lidmaatschappen
WHERE Lidmaatschappen.Einddatum IS NOT NULL AND Lidmaatschappen.Einddatum < enddate;
SET Sql_safe_updates = 1;
COMMIT;
END$$
DELIMITER ;