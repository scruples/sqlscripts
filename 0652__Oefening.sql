USE aptunes;
CREATE USER student IDENTIFIED BY 'ikbeneenstudent';
GRANT EXECUTE ON PROCEDURE GetAlbumDuration TO student;