USE aptunes;
CREATE INDEX TitelIdx
ON Albums (Titel(20));

-- om hier prefix te bepalen moet er toch rekening gehouden worden met
-- vrij veel character aangezien veel titel met "the" etc beginnen?